// Ben Xiong Simple Calculator
#include <iostream>
#include <conio.h>
using namespace std;
//This helps create the equation so the user can type it in the console
float add(float, float);
float sub(float, float);
float mult(float, float);
bool divi(float, float, float);
char o;

float add(float a, float b)
{
    if (o == '+')
        return (a + b);
}
float sub(float a, float b)
{
    if (o == '-')
        return (a - b);
}
float mult(float a, float b)
{
    if (o == '*')
        return (a * b);
}
bool divi(float a, float b, float c)
{
    if (o == '/' && b == 0)
        return (false);
}


int main()
{
    float a, b, c;

    char end = 'y';
    while (end != 'n')
    {
        cout << "Enter a two factor equation with positive numbers: " << endl;
        cin >> a >> o >> b;



        if (o == '+')
        {
            cout << "Answer: " << add(a, b) << endl;
        }

        else if (o == '-')
        {
            cout << "Answer: " << sub(a, b) << endl;


        }

        else if (o == '*')
        {
            cout << "Answer: " << mult(a, b) << endl;
        }

        else if (o == '/' && b == 0)
        {
            cout << "Division by zero is not possible." << endl;
        }
        else
        {
            c = a / b;
            cout << "Answer: " << c << endl;

        }


        cout << "Will you like to continue? Enter y or n: ";
        cin >> end;
    }

    return 0;
    _getch();

}



